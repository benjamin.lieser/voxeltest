#include <iostream>
#include <assert.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include "common/Client.h"
#include "common/World.h"

int main(int argc, char* argv[]) {
    int x = -2576;
    int y = -2345;
    int z = 246456;
    long long id = World::getId(x,y,z);
    int xx = World::getY(id);
    assert(World::getX(id) == x);
    assert(World::getY(id) == y);
    assert(World::getZ(id) == z);
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cerr << "Error: SDLInit: " << SDL_GetError() << std::endl;
        throw Exception("SDLInit failed:");
    }
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);

    SDL_Window* window = SDL_CreateWindow(WindowTitle, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, defaultScreenWidth, defaultScreenHeight , SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
    SDL_GL_CreateContext(window);
    SDL_GL_SetSwapInterval(1);
    //SDL_SetRelativeMouseMode(SDL_TRUE);
    //SDL_SetWindowFullscreen(window,SDL_WINDOW_FULLSCREEN_DESKTOP);
    GLenum glew_status = glewInit();
    if (glew_status != GLEW_OK) {
        std::cerr << "Error: glewInit: " << glewGetErrorString(glew_status) << std::endl;
        throw Exception("GlewInit failed");
    }
    //glEnable(GL_DEBUG_OUTPUT);
    try {
        Client client(window);
        client.start();
    } catch (Exception& e){
        std::cerr << e.what() << std::endl;
    }
    SDL_DestroyWindow(window);
    SDL_Quit();
}