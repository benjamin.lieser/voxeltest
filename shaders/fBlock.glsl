#version 130

varying vec4 fragment;

uniform sampler2DArray texSampler;

const vec4 fogcolor = vec4(1, 1, 1, 1);
const vec4 black = vec4(0,0,0,1);
const float fogdensity = .00003;

void main(void) {
    float z = gl_FragCoord.z / gl_FragCoord.w;
    float fog = clamp(exp(-fogdensity * z * z), 0.2, 1);
    vec4 color = texture(texSampler, fragment.xyz);
    color *= vec4(fragment.www, 1);
    //gl_FragColor = mix(fogcolor, color, fog);
    gl_FragColor = color;
}