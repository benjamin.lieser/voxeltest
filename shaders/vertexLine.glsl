#version 130

attribute vec3 point;
uniform mat4 m_transform;

void main() {
    gl_Position = m_transform * vec4(point, 1);
}
