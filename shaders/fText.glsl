#version 130

varying vec2 texCoord;

uniform sampler2D tex;

void main(void) {
    gl_FragColor = gl_FragColor = vec4(1,0,0,texture2D(tex, texCoord).r);
}