#version 400

layout(location = 0) in vec4 coord; //3d Coords plus light level
layout(location = 1) in uint texInput;

uniform mat4 m_transform;
varying vec4 fragment;

void main(void) {
    gl_Position = m_transform * vec4(coord.xyz, 1);
    fragment.x = float(texInput & uint(1)); //x texture
    fragment.y = float(texInput & uint(2))/2; //y texture
    fragment.z = texInput >> 2; //which texture
    fragment.w = coord.w/255; //light
}