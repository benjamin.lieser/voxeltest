//
// Created by benjamin on 2/25/19.
//

#ifndef GLTEST_VAO_H
#define GLTEST_VAO_H

#include <utility>

#include <GL/glew.h>


class VAO {
public:
    GLuint vao;
    VAO() noexcept {
        glGenVertexArrays(1, &vao);
    }
    ~VAO() noexcept {
        glDeleteVertexArrays(1, &vao);
    }
    VAO(const VAO&) = delete;
    VAO& operator=(const VAO&) = delete;
    VAO(VAO&& other) noexcept : vao(other.vao){
        other.vao = 0;
    }
    VAO& operator=(VAO&& other) noexcept {
        std::swap(vao, other.vao);
        return *this;
    }
};

#endif //GLTEST_VAO_H
