//
// Created by benjamin on 2/12/19.
//
#include "Client.h"

#include <iostream>
#include <chrono>
#include <string>

#include "SDL2/SDL_image.h"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/string_cast.hpp"


#include "loadShader.h"

typedef std::chrono::high_resolution_clock Clock;


Client::Client(SDL_Window* window) : player(0,0,0, world), world(player), camera(0,0,0,world) {
    this->window = window;
    texture = loadTextures();
    chunkProgram = createProgram("shaders/vBlock.glsl", "shaders/fBlock.glsl");
    textProgram = createProgram("shaders/vText.glsl", "shaders/fText.glsl");
    debugProgram = createProgram("shaders/vertexLine.glsl", "shaders/fragmentGreen.glsl");
    projection = glm::perspective(glm::radians(45.0f), (float)screen_width/screen_height, 0.05f, 200.0f);
    if(FT_Init_FreeType(&ft)) {
        throw Exception("Could not load FT Library");
    }
    if(FT_New_Face(ft, "fonts/LiberationSans-Regular.ttf", 0, &font)) {
        throw Exception("Could not load Font");
    }
    textAtlas = new FontTextureAtlas(font, 32);
}

void Client::loadTexture(const char *fileName, GLuint *identifier) {
    SDL_Surface* res_texture = IMG_Load(fileName);
    if (res_texture == nullptr) {
        std::cerr << "IMG_Load: " << SDL_GetError() << std::endl;
        throw Exception("Failure while loading image");
    }
    glGenTextures(1, identifier);
    glBindTexture(GL_TEXTURE_2D, *identifier);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, // target
                 0, // level, 0 = base, no minimap,
                 GL_RGBA, // internalformat
                 res_texture->w, // width
                 res_texture->h, // height
                 0, // border, always 0 in OpenGL ES
                 GL_RGBA, // format
                 GL_UNSIGNED_BYTE, // type
                 res_texture->pixels);
    glGenerateMipmap(GL_TEXTURE_2D);
    SDL_FreeSurface(res_texture);
}

GLuint Client::createProgram(const char *vertexShader, const char *fragmentShader) {
    GLuint program = glCreateProgram();
    GLuint vs = create_shader(vertexShader, GL_VERTEX_SHADER);
    if(vs == 0) {
        print_log(program);
        throw Exception("Could not load Vertex Shader");
    }
    GLuint fs = create_shader(fragmentShader, GL_FRAGMENT_SHADER);
    if(fs == 0) {
        print_log(program);
        throw Exception("Could not load Fragment Shader");
    }
    glAttachShader(program, vs);
    glAttachShader(program, fs);
    glLinkProgram(program);
    GLint link_ok = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
    if (!link_ok) {
        print_log(program);
        throw Exception("Could not link program");
    }
    glDetachShader(program, vs);
    glDetachShader(program, fs);
    glDeleteShader(vs);
    glDeleteShader(fs);
    return program;
}

Client::~Client() {
    glDeleteProgram(chunkProgram);
    glDeleteProgram(debugProgram);
    glDeleteTextures(1, &texture);
    delete textAtlas;
}

void Client::start() {
    auto last_cycle = Clock::now();
    int interframe = 0;
    while (true) {
        SDL_Event ev;

        while (SDL_PollEvent(&ev)) {
            if (ev.type == SDL_QUIT)
                return;
            if (ev.type == SDL_WINDOWEVENT && ev.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
                resize(ev.window.data1, ev.window.data2);
            if (ev.type == SDL_KEYDOWN) {
                switch( ev.key.keysym.sym ){
                    case SDLK_q:
                        return;
                    case SDLK_h:
                        cameraS = !cameraS;
                        break;
                    default:
                        break;
                }
            }
            if (ev.type == SDL_MOUSEMOTION) {
                if(cameraS) {
                    camera.updatePitch(ev.motion.yrel);
                    camera.updateYaw(ev.motion.xrel);
                } else {
                    player.updatePitch(ev.motion.yrel);
                    player.updateYaw(ev.motion.xrel);
                }
            }
            //std::cout << "Time (ms):" << interframe << "\n";
        }
        auto akk = Clock::now();
        long long diff = std::chrono::duration_cast<std::chrono::nanoseconds>(akk - last_cycle).count();
        last_cycle = akk;
        logic(diff);
        interframe = interframe/2 + diff/2000000;
        render(interframe);
    }

}

void Client::resize(int width, int height) {
    screen_width = width;
    screen_height = height;
    projection = glm::perspective(glm::radians(45.0f), (float)screen_width/screen_height, 0.05f, 200.0f);
    glViewport(0, 0, screen_width, screen_height);
}

void Client::render(int interframe) {
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(chunkProgram);
    glActiveTexture(GL_TEXTURE0);
    glUniform1i(glGetUniformLocation(chunkProgram, "texSampler"), 0);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texture);
    if(cameraS) {
        world.render(chunkProgram, projection * camera.getViewMat(), camera.getTranslation());
    } else {
        world.render(chunkProgram, projection * player.getViewMat(), player.getTranslation());
    }


    //Text rendering
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    textAtlas->clear();
    textAtlas->renderText(glm::to_string(player.viewVector()), -1, 0.4, 0.002, 0.002);
    textAtlas->renderText(std::to_string(interframe) + " ms", -1, 0.2, 0.005, 0.005);
    textAtlas->renderText(std::to_string(player.getX()), -1, 0, 0.005, 0.005);
    textAtlas->renderText(std::to_string(player.getY()), -1, -0.2, 0.005, 0.005);
    textAtlas->renderText(std::to_string(player.getZ()), -1, -0.4, 0.005, 0.005);
    textAtlas->makeVAO();
    glUseProgram(textProgram);
    textAtlas->bindTexture(textProgram);
    glBindVertexArray(textAtlas->getVao().vao);
    glDrawArrays(GL_TRIANGLES, 0, textAtlas->getElements());
    glBindVertexArray(0);

    SDL_GL_SwapWindow(window);
}

void Client::logic(long long diff) {
    static float t = 0;
    double time = (double)diff / 1e9;
    if(time > 1) return;
    t += time;
    if(t > 1) {
        t = 0;
        world.updateRenderChunk();
    }
    static const Uint8* keyboard = SDL_GetKeyboardState(nullptr);
    float speed = 7;
    if(keyboard[SDL_SCANCODE_LEFT]) {
        camera.left(time*speed);
    }
    if(keyboard[SDL_SCANCODE_RIGHT]) {
        camera.left(-time*speed);
    }
    if(keyboard[SDL_SCANCODE_UP]) {
        camera.forward(time*speed);
    }
    if(keyboard[SDL_SCANCODE_DOWN]) {
        camera.forward(-time*speed);
    }
    if(keyboard[SDL_SCANCODE_PAGEUP]) {
        camera.up(time*speed);
    }
    if(keyboard[SDL_SCANCODE_PAGEDOWN]) {
        camera.up(-time*speed);
    }
    player.logic(time, keyboard);
}

GLuint Client::loadTextures() {
    GLuint identifier;
    glGenTextures(1, &identifier);
    glBindTexture(GL_TEXTURE_2D_ARRAY, identifier);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, // target
                 0, // level, 0 = base, no minimap,
                 GL_RGBA, // internalformat
                 16, // width
                 16, // height
                 (GLsizei)textureFileNames.size(),
                 0, // border, always 0
                 GL_RGBA, // format
                 GL_UNSIGNED_BYTE, // type
                 nullptr);
    int i = 0;
    for(const auto& filename : textureFileNames) {
        SDL_Surface* res_texture = IMG_Load(("images/blocks/"+filename).c_str());
        if (res_texture == nullptr) {
            std::cerr << "IMG_Load: " << SDL_GetError() << std::endl;
            throw Exception("Failure while loading image");
        }
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
                        0, //level
                        0,
                        0,
                        i,
                        16, 16, 1,
                        GL_RGBA,
                        GL_UNSIGNED_BYTE,
                        res_texture->pixels);
        SDL_FreeSurface(res_texture);
        i++;
    }
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    return identifier;
}
