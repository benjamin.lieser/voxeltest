//
// Created by benjamin on 2/24/19.
//

#include "ChunkVBO.h"
#include "Exceptions.h"

void ChunkVBO::addFace(unsigned char x, unsigned char y, unsigned char z, Face face, unsigned char texture) {
    switch (face) {
        case Face::posX:
            addVertex(x, y, z, 0, 1, 1, 1, texture);
            addVertex(x, y, z, 0, 1, 0, 0, texture);
            addVertex(x, y, z, 0, 1, 1, 0, texture);

            addVertex(x, y, z, 0, 1, 0, 1, texture);
            addVertex(x, y, z, 0, 1, 0, 0, texture);
            addVertex(x, y, z, 0, 1, 1, 1, texture);
            break;
        case Face::negX:
            addVertex(x, y, z, 0, 0, 1, 1, texture);
            addVertex(x, y, z, 0, 0, 1, 0, texture);
            addVertex(x, y, z, 0, 0, 0, 0, texture);

            addVertex(x, y, z, 0, 0, 0, 1, texture);
            addVertex(x, y, z, 0, 0, 1, 1, texture);
            addVertex(x, y, z, 0, 0, 0, 0, texture);
            break;
        case Face::posY:
            addVertex(x, y, z, 1, 0, 1, 1, texture);
            addVertex(x, y, z, 1, 1, 1, 1, texture);
            addVertex(x, y, z, 1, 1, 1, 0, texture);

            addVertex(x, y, z, 1, 0, 1, 1, texture);
            addVertex(x, y, z, 1, 1, 1, 0, texture);
            addVertex(x, y, z, 1, 0, 1, 0, texture);
            break;
        case Face::negY:
            addVertex(x, y, z, 1, 1, 0, 1, texture);
            addVertex(x, y, z, 1, 0, 0, 1, texture);
            addVertex(x, y, z, 1, 1, 0, 0, texture);

            addVertex(x, y, z, 1, 1, 0, 0, texture);
            addVertex(x, y, z, 1, 0, 0, 1, texture);
            addVertex(x, y, z, 1, 0, 0, 0, texture);
            break;
        case Face::posZ:
            addVertex(x, y, z, 2, 0, 0, 1, texture);
            addVertex(x, y, z, 2, 1, 1, 1, texture);
            addVertex(x, y, z, 2, 0, 1, 1, texture);

            addVertex(x, y, z, 2, 0, 0, 1, texture);
            addVertex(x, y, z, 2, 1, 0, 1, texture);
            addVertex(x, y, z, 2, 1, 1, 1, texture);
            break;
        case Face::negZ:
            addVertex(x, y, z, 2, 1, 1, 0, texture);
            addVertex(x, y, z, 2, 0, 0, 0, texture);
            addVertex(x, y, z, 2, 0, 1, 0, texture);

            addVertex(x, y, z, 2, 1, 0, 0, texture);
            addVertex(x, y, z, 2, 0, 0, 0, texture);
            addVertex(x, y, z, 2, 1, 1, 0, texture);
            break;
    }
}

void ChunkVBO::addVertex(unsigned char x, unsigned char y, unsigned char z, unsigned char dir, unsigned char xx, unsigned char yy, unsigned char zz, unsigned char texture) {
    data[dataEnd] = x + xx;
    data[dataEnd + 1] = y + yy;
    data[dataEnd + 2] = z + zz;
    unsigned char light_side = 200;
    unsigned char light_top = 255;
    switch (dir) {
        case 0: //X Side
            data[dataEnd +3] = (light_side);
            data[dataEnd +4] = (zz+2*(yy^1) + 4*texture);
            break;
        case 1: //Y Side
            data[dataEnd +3] =(light_top);
            data[dataEnd +4] = (xx+2*(zz^1) + 4*texture);
            break;
        case 2: //Z Side
            data[dataEnd +3] = (light_side);
            data[dataEnd +4] = (xx+2*(yy^1) + 4*texture);
            break;
        default:
            throw Exception("Invalid dir Value in addVertex");
    }
    dataEnd+=5;
}

void ChunkVBO::makeVAO() {
    glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo);
    glBufferData(GL_ARRAY_BUFFER, dataEnd, data.data(), GL_STATIC_DRAW);
    glBindVertexArray(vao.vao);
    GLuint coord = 0;
    GLuint texInput = 1;
    glEnableVertexAttribArray(coord);
    glEnableVertexAttribArray(texInput);
    glVertexAttribPointer(coord, 4, GL_UNSIGNED_BYTE, GL_FALSE, 5, nullptr);
    glVertexAttribIPointer(texInput, 1, GL_UNSIGNED_BYTE, 5, (void*)4);
    glBindVertexArray(0);
}

GLuint ChunkVBO::getVao() const {
    return vao.vao;
}