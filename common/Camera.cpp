//
// Created by benjamin on 3/4/19.
//

#include "Camera.h"

#include <algorithm>

constexpr float PI = 3.14159265358979323846f;

glm::mat4 Camera::getViewMat() const noexcept {
    glm::mat4 matPitch = glm::mat4(1.0f);
    glm::mat4 matYaw   = glm::mat4(1.0f);

    matPitch = glm::rotate(matPitch, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
    matYaw   = glm::rotate(matYaw,  yaw,    glm::vec3(0.0f, 1.0f, 0.0f));

    return matPitch * matYaw;
}

glm::dvec3 Camera::getTranslation() const noexcept {
    return glm::dvec3(x,y,z);
}

glm::vec3 Camera::getViewVector() const noexcept {
    float radius = std::cos(pitch);
    return -glm::vec3(-radius*std::sin(yaw),std::sin(pitch),radius*std::cos(yaw));
}

void Camera::updatePitch(float diff) noexcept {
    if(diff < 0) {
        pitch = std::max(pitch + diff/100, -PI/2);
    } else {
        pitch = std::min(pitch + diff/100, PI/2);
    }
}

void Camera::updateYaw(float diff) noexcept {
    yaw = std::fmod(yaw + diff/100, 2*PI);
}

void Camera::forward(float diff) noexcept {
    z-=diff*std::cos(yaw);
    x+=diff*std::sin(yaw);
}

void Camera::left(float diff) noexcept {
    z-=diff*std::sin(yaw);
    x-=diff*std::cos(yaw);
}

void Camera::up(float diff) noexcept {
    y+=diff;
}
