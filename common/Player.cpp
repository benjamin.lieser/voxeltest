//
// Created by benjamin on 2/11/19.
//

#include "Player.h"
#include <algorithm>
#include <cmath>
#include <iostream>
#include "World.h"

constexpr float PI = 3.14159265358979323846f;

void Player::updatePitch(float diff) {
    if(diff < 0) {
        pitch = std::max(pitch + diff/100, -PI/2);
    } else {
        pitch = std::min(pitch + diff/100, PI/2);
    }
}

void Player::updateYaw(float diff) {
    yaw = std::fmod(yaw + diff/100, 2*PI);

}

float Player::getX() const {
    return x;
}

float Player::getY() const {
    return y;
}

float Player::getZ() const {
    return z;
}

float Player::getPitch() const {
    return pitch;
}

float Player::getYaw() const {
    return yaw;
}

void Player::forward(float diff) {
    z-=diff*cos(yaw);
    x+=diff*sin(yaw);
}

void Player::left(float diff) {
    z-=diff*sin(yaw);
    x-=diff*cos(yaw);
}

void Player::up(float diff) {
    y+=diff;
}

void Player::print() const {
    std::cout << x << " " << y << " " << z << std::endl;
}

glm::mat4 Player::getViewMat() const {
    glm::mat4 matPitch = glm::mat4(1.0f);
    glm::mat4 matYaw   = glm::mat4(1.0f);

    matPitch = glm::rotate(matPitch, pitch, glm::vec3(1.0f, 0.0f, 0.0f));
    matYaw   = glm::rotate(matYaw,  yaw,    glm::vec3(0.0f, 1.0f, 0.0f));

    //glm::mat4 translate = glm::mat4(1.0f);
    //translate = glm::translate(translate, -glm::vec3(x+wx/2,y+wy,z+wz/2));

    return matPitch * matYaw;
}

void Player::logic(float diff, const Uint8 *keyboardState) {
    //gravity:
    float falldiff = diff*10;
    bool falling = false;
    while(isCollision(0,-falldiff,0) && falldiff >= 0.001) {
        falldiff/=2;
    }
    if(falldiff > 0.001) {
        this->y -= falldiff;
        falling = true;
    }

    if(keyboardState[SDL_SCANCODE_SPACE] && jump == 0 && !falling) { // The player wants to jump and is not already jumping
        jump = 0.4;
    }
    if(jump > 0) { //Player is jumping
        if(jump > diff) { //Full jump
            jump -= diff;
            up(diff*15);
        } else {
            up(jump*15);
            jump = 0;
        }
    }
    if(keyboardState[SDL_SCANCODE_A]) {
        float mov = diff*7;
        left(mov);
        if(isCollision(0,0,0)) {
            left(-mov);
        }
    }
    if(keyboardState[SDL_SCANCODE_D]) {
        float mov = -diff*7;
        left(mov);
        if(isCollision(0,0,0)) {
            left(-mov);
        }
    }
    if(keyboardState[SDL_SCANCODE_W]) {
        float mov = diff*7;
        forward(mov);
        if(isCollision(0,0,0)) {
            forward(-mov);
        }
    }
    if(keyboardState[SDL_SCANCODE_S]) {
        float mov = -diff*7;
        forward(mov);
        if(isCollision(0,0,0)) {
            forward(-mov);
        }
    }
}

bool Player::isCollisionY(float diff) {
    return world.isBlock((int)floor(this->x), (int)floor(this->y + diff), (int)floor(this->z)) || world.isBlock((int)floor(this->x + wx), (int)floor(this->y + diff), (int)floor(this->z + wz))
    || world.isBlock((int)floor(this->x +wx), (int)floor(this->y + diff), (int)floor(this->z)) || world.isBlock((int)floor(this->x), (int)floor(this->y + diff), (int)floor(this->z +wz));
}

bool Player::isCollision(float x, float y, float z) {
    return world.isBlock(this->x+x, this->y + y, this->z + z) || world.isBlock(this->x+x+wx, this->y + y, this->z + z) || world.isBlock(this->x+x, this->y + y, this->z + z+wz) || world.isBlock(this->x+x+wx, this->y + y, this->z + z+wz);
}

int Player::getChunkX() const {
    return (int)x >> 4;
}

int Player::getChunkY() const {
    return (int)y >> 4;
}

int Player::getChunkZ() const {
    return (int)z >> 4;
}

glm::vec3 Player::getTranslation() const {
    return glm::vec3(x+wx/2,y+wy,z+wz/2);
}

glm::vec3 Player::viewVector() const {
    float radius = cos(pitch);
    return -glm::vec3(-radius*sin(yaw),sin(pitch),radius*cos(yaw));
}