//
// Created by benjamin on 2/3/19.
//

#include "Chunk.h"
#include <iostream>

Chunk::Chunk(int x, int y, int z, const FastNoise &generator) : x(x), y(y), z(z) {
    empty = true;
    for (int lx = 0; lx < CHUNKSIZE; lx++) {
        for (int lz = 0; lz < CHUNKSIZE; lz++) {
            auto height = (int) ((generator.GetValue(8*(x * CHUNKSIZE + lx), 8*(z * CHUNKSIZE + lz)) + 1.0) * 8);
            for (int ly = 0; ly < CHUNKSIZE; ly++) {
                data[lx][ly][lz] = y * CHUNKSIZE + ly <= height;
                if(data[lx][ly][lz]) {
                    empty = false;
                }
            }
        }
    }

}

void Chunk::update() {
    changed = false;
    for (unsigned char lx = 0; lx < CHUNKSIZE; lx++) {
        for (unsigned char ly = 0; ly < CHUNKSIZE; ly++) {
            for (unsigned char lz = 0; lz < CHUNKSIZE; lz++) {
                if (data[lx][ly][lz]) {
                    // View from negative x
                    if(lx == 0 || !data[lx-1][ly][lz]) {
                        vbo.addFace(lx,ly,lz,Face::negX, 3);
                    }
                    // View from positive x
                    if(lx == 15 || !data[lx+1][ly][lz]) {
                        vbo.addFace(lx,ly,lz,Face::posX, 3);
                    }
                    // View from negative y
                    if(ly == 0 || !data[lx][ly-1][lz]) {
                        vbo.addFace(lx,ly,lz,Face::negY, 3);
                    }
                    // View from positive y
                    if(ly == 15 || !data[lx][ly+1][lz]) {
                        vbo.addFace(lx,ly,lz,Face::posY, 2);
                    }
                    // View from negative z
                    if(lz == 0 || !data[lx][ly][lz-1]) {
                        vbo.addFace(lx,ly,lz,Face::negZ, 3);
                    }
                    // View from positive z
                    if(lz == 15 || !data[lx][ly][lz+1]) {
                        vbo.addFace(lx,ly,lz,Face::posZ, 3);
                    }
                }
            }
        }
    }
    vbo.makeVAO();
}

void Chunk::render() {
    if(empty) {
        return;
    }
    if(changed) {
        update();
    }
    glBindVertexArray(vbo.getVao());
    glDrawArrays(GL_TRIANGLES, 0, vbo.getElements());
    glBindVertexArray(0);
}

bool Chunk::isBlock(int x, int y, int z) {
    return data[x][y][z];
}
