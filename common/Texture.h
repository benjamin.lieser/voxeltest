//
// Created by benjamin on 3/1/19.
//

#ifndef GLTEST_TEXTURE_H
#define GLTEST_TEXTURE_H

#include <utility>

#include <GL/glew.h>


class Texture {
public:
    GLuint tex;
    Texture() noexcept {
        glGenTextures(1, &tex);
    }
    ~Texture() noexcept {
        glDeleteTextures(1, &tex);
    }
    Texture(const Texture&) = delete;
    Texture& operator=(const Texture&) = delete;
    Texture(Texture&& other) noexcept : tex(other.tex){
        other.tex = 0;
    }
    Texture& operator=(Texture&& other) noexcept {
        std::swap(tex, other.tex);
        return *this;
    }
};

#endif //GLTEST_TEXTURE_H
