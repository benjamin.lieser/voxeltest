//
// Created by benjamin on 2/12/19.
//

#ifndef GLTEST_EXCEPTIONS_H
#define GLTEST_EXCEPTIONS_H

#include <string>

class Exception {
private:
    std::string msg;
public:
    Exception(const char* msg) noexcept : msg(msg) {

    }
    const char* what() const noexcept{
        return msg.c_str();
    }
};


#endif //GLTEST_EXCEPTIONS_H
