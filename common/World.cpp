//
// Created by benjamin on 2/11/19.
//

#include "World.h"
#include "Player.h"
#include <cmath>

long long int World::getId(int x, int y, int z) noexcept{
    unsigned long long mask = (1 << 21) -1;
    unsigned long long xx = x & mask;
    unsigned long long yy = y & mask;
    unsigned long long zz = z & mask;
    return zz << 42 | yy << 21 | xx;
}

int World::getX(long long id) noexcept {
    unsigned long long mask = (1 << 21) -1;
    int temp = (int)((id & mask) << 12);
    return temp >> 12;
}

int World::getY(long long id) noexcept {
    unsigned long long mask = (1 << 21) -1;
    long long temp = (id & (mask << 21)) << 22;
    return (int)(temp >> 43);
}

int World::getZ(long long id) noexcept {
    unsigned long long mask = (1 << 21) -1;
    long long temp = (id & (mask << 42)) << 1;
    return (int)(temp >> 43);
}

bool World::inRange(long long id) const noexcept {
    return abs(player.getChunkX()-getX(id)) <= view_distance && abs(player.getChunkY()-getY(id)) <= view_distance && abs(player.getChunkZ()-getZ(id)) <= view_distance;
}

void World::render(GLuint program, const glm::mat4 &VPMat, const glm::vec3 &translation) {
    for(auto& [id, c] : chunks) {
        glm::vec3 mid(c->x*16 + 8, c->y*16 + 8, c->z*16 +8);
        if(dot((mid - player.getTranslation()+player.viewVector()*16.0f),player.viewVector()) < 0) {
            continue;
        }

        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(c->x*16, c->y*16, c->z*16)-translation);
        glm::mat4 mvp = VPMat * model;

        glUseProgram(program);
        glUniformMatrix4fv(glGetUniformLocation(program, "m_transform"), 1, GL_FALSE, glm::value_ptr(mvp));
        c->render();
    }
}

World::World(Player& player) : player(player), generator(5345) {
    for(int x = player.getChunkX() - view_distance; x <= player.getChunkX() + view_distance; x++) {
        for(int y = -1; y <= 1; y++) {
            for(int z = player.getChunkZ() - view_distance; z <= player.getChunkZ() + view_distance; z++) {
                chunks[getId(x,y,z)] = new Chunk(x,y,z,generator);
            }
        }
    }

}

bool World::isBlock(int x, int y, int z) {
    long long id = getId(x>>4,y>>4,z>>4);
    if(chunks.count(id)) {
        return chunks[id]->isBlock(x&15,y&15,z&15);
    } else {
        return false;
    }
}

bool World::isBlock(float x, float y, float z) {
    return isBlock((int)floor(x), (int)floor(y),(int)floor(z));
}

void World::updateRenderChunk() {
    //Remove old chunks;
    for(auto it = chunks.begin(); it != chunks.end(); ) {
        auto& p = *it;
        if(!inRange(p.first)) {
            delete p.second;
            chunks.erase(it++);
        } else {
            ++it;
        }
    }
    //Get new chunks
    for(int x = player.getChunkX() - view_distance; x <= player.getChunkX() + view_distance; x++) {
        for(int y = -1; y <= 1; y++) {
            for(int z = player.getChunkZ() - view_distance; z <= player.getChunkZ() + view_distance; z++) {
                if(!chunks.count(getId(x,y,z))) {
                    chunks[getId(x,y,z)] = new Chunk(x,y,z,generator);
                }

            }
        }
    }
}

World::~World() noexcept {
    for(auto& [key, val] : chunks) {
        delete val;
    }
}
