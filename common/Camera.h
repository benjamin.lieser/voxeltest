//
// Created by benjamin on 3/4/19.
//

#ifndef GLTEST_CAMERA_H
#define GLTEST_CAMERA_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera {
protected:
    double x,y,z; //Position
    float pitch = 0;
    float yaw = 0;
public:
    glm::mat4 getViewMat() const noexcept;
    glm::dvec3 getTranslation() const noexcept;
    glm::vec3 getViewVector() const noexcept;

    void updatePitch(float diff) noexcept;
    void updateYaw(float diff) noexcept;
    void forward(float diff) noexcept;
    void left(float diff) noexcept;
    void up(float diff) noexcept;
};


#endif //GLTEST_CAMERA_H
