//
// Created by benjamin on 2/25/19.
//

#ifndef GLTEST_VBO_H
#define GLTEST_VBO_H

#include <utility>

#include <GL/glew.h>


class VBO {
public:
    GLuint vbo;
    VBO() noexcept {
        glGenBuffers(1, &vbo);
    }
    ~VBO() noexcept {
        glDeleteBuffers(1, &vbo);
    }
    VBO(const VBO&) = delete;
    VBO& operator=(const VBO&) = delete;
    VBO(VBO&& other) noexcept : vbo(other.vbo){
        other.vbo = 0;
    }
    VBO& operator=(VBO&& other) noexcept {
        std::swap(vbo, other.vbo);
        return *this;
    }
};

#endif //GLTEST_VBO_H
