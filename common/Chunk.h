//
// Created by benjamin on 2/3/19.
//

#ifndef GLTEST_CHUNK_H
#define GLTEST_CHUNK_H

#include "FastNoise.h"
#include <GL/glew.h>

#include "ChunkVBO.h"

#define CHUNKSIZE 16

class Chunk {
public:
    int x,y,z; //Koordinates of the chunk
    bool data[CHUNKSIZE][CHUNKSIZE][CHUNKSIZE];
    bool original = true;
    bool changed = true;
    bool empty; //Is this chunk empty
    int elements = 0;
    Chunk(int x, int y, int z, const FastNoise &generator);
    void update();
    void render();
    bool isBlock(int x, int y, int z);

private:
    ChunkVBO vbo;
};


#endif //GLTEST_CHUNK_H
