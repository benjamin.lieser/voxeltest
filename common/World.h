//
// Created by benjamin on 2/11/19.
//

#ifndef GLTEST_WORLD_H
#define GLTEST_WORLD_H

#include <unordered_map>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Chunk.h"
#include "FastNoise.h"
#include <GL/glew.h>
#include "Player.h"

class World {
private:
    int view_distance = 10;
    Player& player;
    FastNoise generator;
    std::unordered_map<long long, Chunk*> chunks;
    std::unordered_map<long long, Chunk*> unrenderedChunks;
    bool inRange(long long id) const noexcept;

public:
    static long long int getId(int x, int y, int z) noexcept;
    static int getX(long long id) noexcept;
    static int getY(long long id) noexcept;
    static int getZ(long long id) noexcept;
    void updateRenderChunk();
    World(Player& player);
    ~World() noexcept;
    void render(GLuint program, const glm::mat4 &VPMat, const glm::vec3 &translation);
    bool isBlock(int x, int y, int z);
    bool isBlock(float x, float y, float z);
};


#endif //GLTEST_WORLD_H
