//
// Created by benjamin on 2/24/19.
//

#ifndef GLTEST_CHUNKVBO_H
#define GLTEST_CHUNKVBO_H

#include <GL/glew.h>
#include <vector>

#include "VBO.h"
#include "VAO.h"

enum class Face {posX, negX, posY, negY, posZ, negZ};

class ChunkVBO {
public:
    ChunkVBO() {
        data.resize(400000);
    }
    void addFace(unsigned char x, unsigned char y, unsigned char z, Face face, unsigned char texture);
    void makeVAO();
    GLuint getVao() const;
    int getElements() const {
        return dataEnd/5;
    }

private:
    VBO vbo;
    VAO vao;
    int dataEnd = 0;
    std::vector<unsigned char> data;
    void addVertex(unsigned char x, unsigned char y, unsigned char z, unsigned char dir, unsigned char xx,
                   unsigned char yy, unsigned char zz, unsigned char texture);
};


#endif //GLTEST_CHUNKVBO_H
