//
// Created by benjamin on 3/1/19.
//

#include "FontTextureAtlas.h"

#include <algorithm>
#include <iostream>

FontTextureAtlas::FontTextureAtlas(FT_Face &face, unsigned int size) {
    FT_Set_Pixel_Sizes(face, 0, size);
    FT_GlyphSlot g = face->glyph;
    unsigned int w = 0;
    unsigned int h = 0;
    for(unsigned int i = 32; i < 128; i++) {
        if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
            //Error while loading, we just ignore the missing character
            continue;
        }
        info[i-32] = {(float)g->advance.x / 64, (float)g->advance.y / 64, (float)g->bitmap.width, (float)g->bitmap.rows, (float)g->bitmap_left, (float)g->bitmap_top, 0};
        h = std::max(h, g->bitmap.rows);
        w += g->bitmap.width+2; //Space
    }
    glActiveTexture(GL_TEXTURE0 +1);
    glBindTexture(GL_TEXTURE_2D, atlas.tex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    texHeight = h;
    texWidth = w;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, w, h, 0, GL_RED, GL_UNSIGNED_BYTE, nullptr);

    int x = 0;

    for(unsigned int i = 32; i < 128; i++) {
        if(FT_Load_Char(face, i, FT_LOAD_RENDER)) {
            continue;
        }
        if(g->bitmap.buffer) {
            glTexSubImage2D(GL_TEXTURE_2D, 0, x, 0, g->bitmap.width, g->bitmap.rows, GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer);
        }
        info[i-32].tx = (float)x / w;
        x += g->bitmap.width+2;
    }
    glGenerateMipmap(GL_TEXTURE_2D);
}

void FontTextureAtlas::renderText(const std::string &text, float x, float y, float sx, float sy) {
    for(char p : text) {
        float x2 = x + info[p-32].bl*sx;
        float y2 = y + info[p-32].bt*sy;
        float w = info[p-32].bw * sx;
        float h = info[p-32].bh * sy;

        x += info[p-32].ax * sx;
        y += info[p-32].ay * sy;
        float xPos = info[p-32].tx;
        float xDelta = info[p-32].bw/texWidth;
        float height = info[p-32].bh/texHeight;
        data.push_back({x2,y2,xPos, 0});
        data.push_back({x2 + w,y2,xPos+xDelta, 0});
        data.push_back({x2,y2 - h,xPos, height});

        data.push_back({x2 + w,y2,xPos+xDelta, 0});
        data.push_back({x2,y2-h,xPos, height});
        data.push_back({x2+w,y2-h,xPos+ xDelta, height});
    }
}

void FontTextureAtlas::makeVAO() {
    glBindBuffer(GL_ARRAY_BUFFER, vbo.vbo);
    glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(vertex), data.data(), GL_DYNAMIC_DRAW);
    glBindVertexArray(vao.vao);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
    glBindVertexArray(0);
}

void FontTextureAtlas::bindTexture(GLuint program) {
    glActiveTexture(GL_TEXTURE1);
    glUniform1i(glGetUniformLocation(program, "tex"), 1);
    glBindTexture(GL_TEXTURE_2D, atlas.tex);
}

const VAO &FontTextureAtlas::getVao() const {
    return vao;
}

int FontTextureAtlas::getElements() {
    return (int)data.size();
}

void FontTextureAtlas::clear() {
    data.clear();
}
