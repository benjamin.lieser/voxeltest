//
// Created by benjamin on 3/1/19.
//

#ifndef GLTEST_FONTTEXTUREATLAS_H
#define GLTEST_FONTTEXTUREATLAS_H

#include <string>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Texture.h"
#include "VAO.h"
#include "VBO.h"

class FontTextureAtlas {
public:
    FontTextureAtlas(FT_Face &face, unsigned int size);
    void renderText(const std::string &text, float x, float y, float sx, float sy);
    void makeVAO();
    void bindTexture(GLuint program);
    int getElements();
    void clear();
    struct vertex {
        float x;
        float y;
        float texX;
        float texY;
    };
private:
    int texHeight;
    int texWidth;
    Texture atlas;
    struct GlyphInfo {
        float ax; // advance.x
        float ay; // advance.y

        float bw; // bitmap.width;
        float bh; // bitmap.rows;

        float bl; // bitmap_left;
        float bt; // bitmap_top;

        float tx; // x offset of glyph in texture coordinates
    };
    GlyphInfo info[96]; // Space(0x20) up to Tilde(0x7E)
    std::vector<vertex> data;
    VAO vao;
public:
    const VAO &getVao() const;

private:
    VBO vbo;
};


#endif //GLTEST_FONTTEXTUREATLAS_H
