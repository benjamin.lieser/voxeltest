//
// Created by benjamin on 2/1/19.
//

#ifndef GLTEST_LOADSHADER_H
#define GLTEST_LOADSHADER_H
GLuint create_shader(const char* filename, const GLenum type);
void print_log(GLuint object);
char* readFile(const char* filename);
#endif //GLTEST_LOADSHADER_H
