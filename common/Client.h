//
// Created by benjamin on 2/12/19.
//

#ifndef GLTEST_CLIENT_H
#define GLTEST_CLIENT_H

#include <string>
#include <vector>

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "Exceptions.h"
#include "Player.h"
#include "World.h"
#include "FontTextureAtlas.h"

constexpr int defaultScreenWidth = 800;
constexpr int defaultScreenHeight = 600;
constexpr char WindowTitle[] = "I need a Name";


/* The Client Class represents the Game, it manages the Window, the OpenGL Context, the Game Loop, User Input
 *
 */
class Client {
public:
    // Constructor which default constructs the Client, it inits SDL and OpenGL, creates a Window
    Client(SDL_Window* window);

    //Disable any sort of copy
    Client(const Client&) = delete;
    Client(Client&&) = delete;
    Client& operator=(const Client&) = delete;
    Client& operator=(Client&&) = delete;

    ~Client();
    void start(); //Starts the game

private:
    Player player;
    Player camera;
    bool cameraS = false;
    glm::mat4 projection;
    World world;
    SDL_Window* window;
    GLuint texture;

    GLuint chunkProgram;
    GLuint debugProgram;
    GLuint textProgram;
    int screen_width = defaultScreenWidth;
    int screen_height = defaultScreenHeight;
    static void loadTexture(const char* fileName, GLuint* identifier);
    GLuint loadTextures();
    static GLuint createProgram(const char* vertexShader, const char* fragmentShader);
    void resize(int screen_width, int screen_height);
    void render(int interframe);
    //diff: Time in nanoseconds
    void logic(long long diff);
    const std::vector<std::string> textureFileNames {"stone.png", "dirt.png", "grass_top.png", "grass_side.png"};

    FT_Library ft;
    FT_Face font;
    FontTextureAtlas* textAtlas;

};


#endif //GLTEST_CLIENT_H
