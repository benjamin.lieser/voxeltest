//
// Created by benjamin on 2/11/19.
//

#ifndef GLTEST_PLAYER_H
#define GLTEST_PLAYER_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <SDL2/SDL.h>

#include "Camera.h"

class World;

class Player : public Camera{
private:
    float speed = 0.150;
    //Is the Player jumping, when 0 it is not jumping, otherwise it gives the time until it reaches maximum height
    float jump = 0;
    World& world;
    float wx = 0.5; //Width in x direction
    float wz = 0.5; //Width in z direction
    float wy = 1.5; //Width in y direction (Height)
    bool isCollisionY(float diff);
    bool isCollision(float x, float y, float z);
public:
    float getX() const;
    int getChunkX() const;
    float getY() const;
    int getChunkY() const;
    float getZ() const;
    int getChunkZ() const;
    float getPitch() const;
    glm::vec3 getTranslation() const;
    glm::vec3 viewVector() const;
    float getYaw() const;

    Player(float x, float y, float z, World& world) : x(x), y(y), z(z), pitch(0), yaw(0), world(world) {

    }

    void updatePitch(float diff);

    void updateYaw(float diff);

    void forward(float diff);

    void left(float diff);

    void up(float diff);

    //Time in seconds
    void logic(float diff, const Uint8* keyboardState);

    glm::mat4 getViewMat() const;

    void print() const;
};


#endif //GLTEST_PLAYER_H
