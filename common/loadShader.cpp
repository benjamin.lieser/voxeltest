//
// Created by benjamin on 2/1/19.
//
#include <fstream>
#include <sstream>
#include <cstring>
#include <GL/glew.h>
#include <iostream>
#include "loadShader.h"

char* readFile(const char* filename) {
    std::ifstream in;
    in.open(filename);
    if(!in) {
        std::cerr << "Could not open " << filename << std::endl;
        return nullptr;
    }
    std::stringstream ss;
    ss << in.rdbuf();
    size_t size = ss.str().size();
    auto* result = new char[size+1];
    std::strcpy(result, ss.str().c_str());
    return result;
}

/**
 * Display compilation errors from the OpenGL shader compiler
 */
void print_log(GLuint object) {
    GLint log_length = 0;
    if (glIsShader(object)) {
        glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
    } else if (glIsProgram(object)) {
        glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
    } else {
        std::cerr << "printlog: Not a shader or a program" << std::endl;
        return;
    }

    auto* log = (char*)malloc(log_length);

    if (glIsShader(object))
        glGetShaderInfoLog(object, log_length, nullptr, log);
    else if (glIsProgram(object))
        glGetProgramInfoLog(object, log_length, nullptr, log);

    std::cerr << log;
    free(log);
}

GLuint create_shader(const char* filename, const GLenum type) {
    const GLchar* source = readFile(filename);
    GLuint res = glCreateShader(type);
    glShaderSource(res, 1, &source, nullptr);
    delete[] source;
    glCompileShader(res);
    GLint compile_ok = GL_FALSE;
    glGetShaderiv(res, GL_COMPILE_STATUS, &compile_ok);
    if (compile_ok == GL_FALSE) {
        std::cerr << filename << ":";
        print_log(res);
        glDeleteShader(res);
        return 0;
    }
    return res;
}